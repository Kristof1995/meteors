//
//  FavoritesViewModelTests.swift
//  FavoritesViewModelTests
//
//  Created by Kristof Varga on 2021. 07. 29..
//

import XCTest
import Combine
@testable import Meteors

class FavoritesViewModelTests: XCTestCase {

    var viewModel: FavoritesViewModel!

    var favoriteRepository: TestFavoriteRepository!

    var cancellables: Set<AnyCancellable> = []

    override func setUp() {
        self.favoriteRepository = TestFavoriteRepository()
        self.viewModel = FavoritesViewModel(favoriteRepository: favoriteRepository)
    }

    func testEmpty() {
        let expectation = XCTestExpectation(description: "empty")

        self.favoriteRepository.meteors = []

        viewModel.$favorites
            .dropFirst()
            .sink(
                receiveValue: { value in
                    expectation.fulfill()
                }
            )
            .store(in: &cancellables)

        viewModel.onAppear()

        wait(for: [expectation], timeout: 0.5)

        XCTAssert(viewModel.favorites.isEmpty)
        XCTAssertFalse(viewModel.message.isEmpty)
    }

    func testOne() {
        let expectation = XCTestExpectation(description: "empty")

        self.favoriteRepository.meteors = [TestMeteor(meteorId: "id", meteorName: "name", mass: nil, lat: 0, lon: 0, year: nil)]

        viewModel.$favorites
            .dropFirst()
            .sink(
                receiveValue: { value in
                    expectation.fulfill()
                }
            )
            .store(in: &cancellables)

        viewModel.onAppear()

        wait(for: [expectation], timeout: 0.5)

        XCTAssertEqual(viewModel.favorites.count, 1)
        XCTAssertEqual( viewModel.favorites.first!.meteorName, "name")
        XCTAssert(viewModel.message.isEmpty)
    }
}
