//
//  MeteorListViewModelTests.swift
//  MeteorListViewModelTests
//
//  Created by Kristof Varga on 2021. 07. 29..
//

import XCTest
import Combine
@testable import Meteors

class MeteorListViewModelTests: XCTestCase {
    
    var viewModel: MeteorListViewModel!
    var meteorProvider: TestMeteorProvider!

    var cancellables: Set<AnyCancellable> = []

    override func setUp() {

        self.meteorProvider = TestMeteorProvider()
        self.viewModel = MeteorListViewModel(meteorProvider: self.meteorProvider)
    }

    func testEmpty() {

        let expectation = XCTestExpectation(description: "empty")


        self.meteorProvider.meteors = []

        viewModel.$meteors
            .sink(
                receiveValue: { value in
                    expectation.fulfill()
                    XCTAssert(value.isEmpty)
                }
            )
            .store(in: &cancellables)

        viewModel.onAppear()

        wait(for: [expectation], timeout: 0.5)
    }

    func testOneMeteor() {
        let expectation = XCTestExpectation(description: "one meteor")
        expectation.expectedFulfillmentCount = 3

        self.meteorProvider.meteors = [
            TestMeteor(meteorId: "1", meteorName: "Test", mass: nil, lat: 1, lon: 1, year: Date())
        ]

        viewModel.$meteors
            .sink(
                receiveValue: { _ in
                    expectation.fulfill()
                }
            )
            .store(in: &cancellables)

        viewModel.onAppear()

        wait(for: [expectation], timeout: 0.5)

        XCTAssertEqual(viewModel.meteors.count, 1)
        XCTAssertEqual(viewModel.meteors.first!.meteorId, "1")
        XCTAssertEqual(viewModel.message, "")
        XCTAssertFalse(viewModel.isLoading)
    }

    func testMultipleMeteor() {
        let expectation = XCTestExpectation(description: "two meteor")
        expectation.expectedFulfillmentCount = 3

        self.meteorProvider.meteors = [
            TestMeteor(meteorId: "1", meteorName: "Test", mass: nil, lat: 1, lon: 1, year: Date()),
            TestMeteor(meteorId: "2", meteorName: "Test2", mass: "700", lat: 1, lon: 1, year: Date())
        ]

        viewModel.$meteors
            .sink(
                receiveValue: { _ in
                    expectation.fulfill()
                }
            )
            .store(in: &cancellables)

        viewModel.onAppear()

        wait(for: [expectation], timeout: 0.5)

        XCTAssertEqual(viewModel.meteors.count, 2)
        XCTAssertEqual(viewModel.meteors.first!.meteorId, "1")
        XCTAssertEqual(viewModel.message, "")
        XCTAssertFalse(viewModel.isLoading)
    }

    func testError() {
            let expectation = XCTestExpectation(description: "error")
            expectation.expectedFulfillmentCount = 3

        self.meteorProvider.error = TestError.testError

            viewModel.$meteors
                .sink(
                    receiveValue: { _ in
                        expectation.fulfill()
                    }
                )
            .store(in: &cancellables)

        viewModel.onAppear()

        wait(for: [expectation], timeout: 0.5)

        XCTAssertEqual(viewModel.meteors.count, 0)
        XCTAssertFalse(viewModel.message.isEmpty)
        XCTAssertFalse(viewModel.isLoading)
    }

    func testSortingByMass() {
        let expectation = XCTestExpectation(description: "sorting by mass")

        self.meteorProvider.meteors = [
            TestMeteor(meteorId: "1", meteorName: "Test", mass: nil, lat: 1, lon: 1, year: Date())
        ]

        self.meteorProvider.expectation = expectation

        viewModel.onAppear()

        viewModel.sorting = .mass

        wait(for: [expectation], timeout: 0.5)

        XCTAssertNotNil(self.meteorProvider.orderQuery)
        if let orderQuery = self.meteorProvider.orderQuery {
            switch orderQuery {
            case .mass(isAscending: let isAscending):
                XCTAssert(isAscending)
            default:
                XCTFail("the orderQuery is not mass")
            }
        }
    }

    func testDescendingOrder() {
        let expectation = XCTestExpectation(description: "descending name")

        self.meteorProvider.meteors = [
            TestMeteor(meteorId: "1", meteorName: "Test", mass: nil, lat: 1, lon: 1, year: Date())
        ]

        self.meteorProvider.expectation = expectation

        viewModel.onAppear()

        viewModel.isAscending = false

        wait(for: [expectation], timeout: 0.5)

        XCTAssertNotNil(self.meteorProvider.orderQuery)
        if let orderQuery = self.meteorProvider.orderQuery {
            switch orderQuery {
            case .name(isAscending: let isAscending):
                XCTAssertFalse(isAscending)
            default:
                XCTFail("the orderQuery is not name")
            }
        }
    }

    func testFirstPage() {
        for page in 0...5 {
            pagination(page: page)
        }
    }

    func pagination(page: Int) {
        let expectation = XCTestExpectation(description: "test pagination. page: \(page)")

        self.meteorProvider.meteors = [
            TestMeteor(meteorId: "1", meteorName: "Test", mass: nil, lat: 1, lon: 1, year: Date())
        ]

        self.meteorProvider.expectation = expectation

        viewModel.onAppear()

        for _ in 0..<page {
            viewModel.lastItemAppeared()
        }

        wait(for: [expectation], timeout: 0.5)

        guard let limit = self.meteorProvider.limit, let offset = self.meteorProvider.offset else {
            XCTFail()
            return
        }

        XCTAssert(offset % limit == 0)
        XCTAssertEqual(offset, limit * page )
    }
}
