//
//  MeteorDetailViewModelTests.swift
//  MeteorListViewModelTests
//
//  Created by Kristof Varga on 2021. 07. 29..
//

import XCTest
import Combine
@testable import Meteors

class MeteorDetailViewModelTests: XCTestCase {

    var viewModel: MeteorDetailViewModel!

    var meteorProvider: TestMeteorProvider!
    var favoriteRepository: TestFavoriteRepository!

    var cancellables: Set<AnyCancellable> = []

    let id = "testId"

    override func setUp() {

        self.meteorProvider = TestMeteorProvider()
        self.favoriteRepository = TestFavoriteRepository()
        self.viewModel = MeteorDetailViewModel(meteorId: id, meteorProvider: meteorProvider, favoriteRepository: favoriteRepository)
    }

    func testMeteor() {
        let expectation = XCTestExpectation(description: "meteor")
        expectation.expectedFulfillmentCount = 2

        self.meteorProvider.meteor = TestMeteor(meteorId: id, meteorName: "Test", mass: nil, lat: 1, lon: 1, year: Date())

        viewModel.$meteor
            .sink(
                receiveValue: { _ in
                    expectation.fulfill()
                }
            )
            .store(in: &cancellables)

        viewModel.onAppear()

        wait(for: [expectation], timeout: 0.5)

        XCTAssertNotNil(viewModel.meteor)
        XCTAssertEqual(viewModel.meteor!.meteorId, id)
        XCTAssertFalse(viewModel.isLoading)
    }

    func testError() {
        let expectation = XCTestExpectation(description: "error")
        

        self.meteorProvider.error = TestError.testError

        viewModel.$message
            .dropFirst()
            .sink(receiveValue: { _ in
                expectation.fulfill()
            })
            .store(in: &cancellables)

        viewModel.onAppear()

        wait(for: [expectation], timeout: 5.5)

        XCTAssertNil(viewModel.meteor)
        XCTAssertFalse(viewModel.isLoading)
        XCTAssertFalse(viewModel.message.isEmpty)
    }

    func testFavorite() {
        let providerExpectation = XCTestExpectation(description: "provider")
        let favoriteRepositoryExpectation = XCTestExpectation(description: "repository")


        let testMeteor = TestMeteor(meteorId: id, meteorName: "Test", mass: nil, lat: 1, lon: 1, year: Date())

        self.meteorProvider.meteor = testMeteor
        self.meteorProvider.expectation = providerExpectation

        self.favoriteRepository.meteor = testMeteor
        self.favoriteRepository.expectation = favoriteRepositoryExpectation

        viewModel.onAppear()

        wait(for: [providerExpectation, favoriteRepositoryExpectation], timeout: 0.5)

        XCTAssert(viewModel.isFavorite)
    }

    func testNoFavorite() {
        let providerExpectation = XCTestExpectation(description: "provider")
        let favoriteRepositoryExpectation = XCTestExpectation(description: "repository")


        let testMeteor = TestMeteor(meteorId: id, meteorName: "Test", mass: nil, lat: 1, lon: 1, year: Date())

        self.meteorProvider.meteor = testMeteor
        self.meteorProvider.expectation = providerExpectation

        self.favoriteRepository.meteor = nil
        self.favoriteRepository.expectation = favoriteRepositoryExpectation

        viewModel.onAppear()

        wait(for: [providerExpectation, favoriteRepositoryExpectation], timeout: 0.5)

        XCTAssertFalse(viewModel.isFavorite)
    }

    func testSetFavorite() {

        let favoriteRepositoryExpectation = XCTestExpectation(description: "repository")

        self.favoriteRepository.meteor = nil
        self.favoriteRepository.expectation = favoriteRepositoryExpectation

        viewModel.meteor = TestMeteor(meteorId: id, meteorName: "Test", mass: "700", lat: 1, lon: 1, year: Date())
        viewModel.isFavorite = false
        viewModel.toggleFavorite()

        wait(for: [favoriteRepositoryExpectation], timeout: 0.5)

        XCTAssertNotNil(favoriteRepository.meteor)
    }

    func testRemoveFavorite() {

        let favoriteRepositoryExpectation = XCTestExpectation(description: "repository")

        let testmeteor = TestMeteor(meteorId: id, meteorName: "Test", mass: "700", lat: 1, lon: 1, year: Date())

        self.favoriteRepository.meteor = testmeteor
        self.favoriteRepository.expectation = favoriteRepositoryExpectation

        viewModel.meteor = testmeteor
        viewModel.isFavorite = true
        viewModel.toggleFavorite()

        wait(for: [favoriteRepositoryExpectation], timeout: 0.5)

        XCTAssertNil(favoriteRepository.meteor)
    }
}
