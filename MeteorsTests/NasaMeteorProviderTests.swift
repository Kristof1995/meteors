//
//  NasaMeteorProviderTests.swift
//  MeteorsTests
//
//  Created by Kristof Varga on 2021. 08. 02..
//

import XCTest
import Combine
@testable import Meteors

class NasaMeteorProviderTests: XCTestCase {

    var provider: NasaMeteorProvider!

    var networkConnector: TestNetworkConnector<NasaMeteor>!

    private var cancellables: Set<AnyCancellable> = []

    override func setUp() {
        self.networkConnector = .init()
        self.provider = .init(baseUrl: URL(string:"https://baseurl.com")!, networkConnector: networkConnector)
    }

    func testFetchByIdError() {

        let expectation = expectation(description: "")
        networkConnector.error = TestError.testError

        provider.fetchMeteor(byId: "1")
            .sink { completion in
                expectation.fulfill()
                let completion = completion

                switch completion {
                case .failure(let error):
                    XCTAssertEqual(error.localizedDescription, TestError.testError.localizedDescription)
                case .finished:
                    XCTFail("should fail")
                }
            } receiveValue: { _ in

            }
            .store(in: &cancellables)

        wait(for: [expectation], timeout: 0.5)
    }

    func testFetchById() {

        let expectation = expectation(description: "")

        networkConnector.resultArray = [
            NasaMeteor(name: "name", id: "1", nametype: "", recclass: "", mass: nil, fall: "", year: nil, reclat: nil, reclong: nil, geolocation: nil)
        ]

        provider.fetchMeteor(byId: "1")
            .sink(receiveCompletion: { _ in

            }, receiveValue: { meteor in
                expectation.fulfill()
                XCTAssertEqual(meteor.meteorName, "name")
            })
            .store(in: &cancellables)

        wait(for: [expectation], timeout: 0.5)
    }

    func testFetchAll() {

        let expectation = expectation(description: "")

        networkConnector.resultArray = [
            NasaMeteor(name: "name", id: "1", nametype: "", recclass: "", mass: nil, fall: "", year: nil, reclat: nil, reclong: nil, geolocation: nil),
            NasaMeteor(name: "name2", id: "2", nametype: "", recclass: "", mass: nil, fall: "", year: nil, reclat: nil, reclong: nil, geolocation: nil)
        ]

        var meteors: [Meteor]?

        provider.fetchMeteors(orderQuery: .name(isAscending: true), limit: 10, offset: 0)
            .sink(receiveCompletion: { _ in

            }, receiveValue: { result in
                expectation.fulfill()
                meteors = result
            })
            .store(in: &cancellables)

        wait(for: [expectation], timeout: 0.5)

        XCTAssertEqual(meteors?.count, 2)
        XCTAssertEqual(meteors?[0].meteorName, "name")
        XCTAssertEqual(meteors?[1].meteorName, "name2")
    }

    func testNameRequest() {

        let expectation = expectation(description: "")

        networkConnector.expectation = expectation

        provider.fetchMeteors(orderQuery: .name(isAscending: true), limit: 10, offset: 0)
            .sink(receiveCompletion: { _ in

            }, receiveValue: { _ in

            })
            .store(in: &cancellables)

        wait(for: [expectation], timeout: 0.5)

        XCTAssertNotNil(networkConnector.request)
        XCTAssertEqual(networkConnector.request!.url!.absoluteString, "https://baseurl.com?$where=year%20%3E%20'1900-01-01T00:00:00.000'&$order=name%20ASC&$limit=10&$offset=0")
    }

    func testMassRequest() {

        let expectation = expectation(description: "")

        networkConnector.expectation = expectation

        provider.fetchMeteors(orderQuery: .mass(isAscending: false), limit: 10, offset: 0)
            .sink(receiveCompletion: { _ in

            }, receiveValue: { _ in

            })
            .store(in: &cancellables)

        wait(for: [expectation], timeout: 0.5)

        XCTAssertNotNil(networkConnector.request)
        XCTAssertEqual(networkConnector.request!.url!.absoluteString, "https://baseurl.com?$where=year%20%3E%20'1900-01-01T00:00:00.000'&$order=mass%20DESC&$limit=10&$offset=0")
    }

    func testDistanceRequest() {

        let expectation = expectation(description: "")

        networkConnector.expectation = expectation

        provider.fetchMeteors(orderQuery: .location(location: (lat: 11, lon: 11), isAscending: false), limit: 10, offset: 30)
            .sink(receiveCompletion: { _ in

            }, receiveValue: { _ in

            })
            .store(in: &cancellables)

        wait(for: [expectation], timeout: 0.5)

        XCTAssertNotNil(networkConnector.request)
        XCTAssertEqual(networkConnector.request!.url!.absoluteString, "https://baseurl.com?$where=year%20%3E%20'1900-01-01T00:00:00.000'&$order=DISTANCE_IN_METERS(geolocation,%20'POINT(11.0%2011.0)')%20DESC&$limit=10&$offset=30")
    }
}
