//
//  CoreDataFavoriteRepositoryTests.swift
//  MeteorsTests
//
//  Created by Kristof Varga on 2021. 08. 03..
//

import XCTest
import Combine
import CoreData
@testable import Meteors

class CoreDataFavoriteRepositoryTests: XCTestCase {

    var repository: CoreDataFavoriteRepository!

    lazy var persistentContainer: NSPersistentContainer = {
            let description = NSPersistentStoreDescription()
            description.url = URL(fileURLWithPath: "/dev/null")
            let container = NSPersistentContainer(name: "Favorites")
            container.persistentStoreDescriptions = [description]
            container.loadPersistentStores { _, error in
                if let error = error as NSError? {
                    fatalError("Unresolved error \(error), \(error.userInfo)")
                }
            }
            return container
        }()

    var cancellables: Set<AnyCancellable> = []

    override func setUp() {

        self.repository = .init(persistentContainer: persistentContainer)
    }

    func testEmpty() {
        let expectation = expectation(description: "")

        repository.fetchAll()
            .sink { _ in

            } receiveValue: { meteors in
                expectation.fulfill()

                XCTAssert(meteors.isEmpty)
            }.store(in: &cancellables)

        wait(for: [expectation], timeout: 0.5)

    }

    func testNotEmpty() {

        let context = persistentContainer.newBackgroundContext()
        let didSaveExpectation = expectation(forNotification: .NSManagedObjectContextDidSave,
                    object: context) { _ in
            return true
        }

        for i in 0..<5 {
            createTestMeteor(number: i, context: context)
        }

        try! context.save()

        wait(for: [didSaveExpectation], timeout: 1.0)

        let expectation = expectation(description: "")

        repository.fetchAll()
            .sink { _ in

            } receiveValue: { meteors in
                expectation.fulfill()

                XCTAssertFalse(meteors.isEmpty)
                XCTAssertEqual(meteors.count, 5)

                let sorted = meteors.sorted(by: { $0.meteorId < $1.meteorId })

                for i in 0..<5 {
                    XCTAssertEqual(sorted[i].meteorId, "\(i)")
                    XCTAssertEqual(sorted[i].mass, "\(i)")
                    XCTAssertEqual(sorted[i].meteorName, "\(i)")
                    XCTAssertEqual(sorted[i].lat, Double(i))
                    XCTAssertEqual(sorted[i].lon, Double(i))
                    XCTAssertNotNil(sorted[i].year)
                }

            }.store(in: &cancellables)

        wait(for: [expectation], timeout: 0.5)
    }

    func testFetchByIdNotExisting() {
        let context = persistentContainer.newBackgroundContext()
        let didSaveExpectation = expectation(forNotification: .NSManagedObjectContextDidSave,
                    object: context) { _ in
            return true
        }

        for i in 0..<5 {
            createTestMeteor(number: i, context: context)
        }

        try! context.save()

        wait(for: [didSaveExpectation], timeout: 1.0)

        let expectation = expectation(description: "")

        repository.fetchById(id: "notExistingId")
            .sink { _ in

            } receiveValue: { meteor in
                expectation.fulfill()
                XCTAssertNil(meteor)

            }.store(in: &cancellables)

        wait(for: [expectation], timeout: 0.5)
    }

    func testFetchById() {
        let context = persistentContainer.newBackgroundContext()
        let didSaveExpectation = expectation(forNotification: .NSManagedObjectContextDidSave,
                    object: context) { _ in
            return true
        }

        for i in 0..<5 {
            createTestMeteor(number: i, context: context)
        }

        try! context.save()

        wait(for: [didSaveExpectation], timeout: 1.0)

        let expectation = expectation(description: "")

        repository.fetchById(id: "1")
            .sink { _ in

            } receiveValue: { meteor in
                expectation.fulfill()
                XCTAssertNotNil(meteor)
                XCTAssertEqual(meteor?.meteorId, "1")

            }.store(in: &cancellables)

        wait(for: [expectation], timeout: 0.5)
    }


    func testAdd() {
        let context = persistentContainer.newBackgroundContext()
        let didSaveExpectation = expectation(forNotification: .NSManagedObjectContextDidSave, object: nil) { _ in
            return true
        }

        repository.add(meteor: TestMeteor(meteorId: "1", meteorName: "1", mass: "1", lat: 1, lon: 1, year: Date()))

        wait(for: [didSaveExpectation], timeout: 0.5)

        let request: NSFetchRequest<CDFavoriteMeteor> = CDFavoriteMeteor.fetchRequest()
        let result = try! context.fetch(request)

        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(result.first?.id, "1")
        XCTAssertEqual(result.first?.name, "1")
        XCTAssertEqual(result.first?.mass, "1")
        XCTAssertEqual(result.first?.lat, 1)
        XCTAssertEqual(result.first?.lon, 1)
        XCTAssertNotNil(result.first?.year)
    }

    func testDelete() {
        let context = persistentContainer.newBackgroundContext()
        let didSaveExpectation = expectation(forNotification: .NSManagedObjectContextDidSave,
                    object: context) { _ in
            return true
        }

        createTestMeteor(number: 1, context: context)
        try! context.save()
        wait(for: [didSaveExpectation], timeout: 1.0)

        let didDeleteExpectation = expectation(forNotification: .NSManagedObjectContextDidSave, object: nil) { _ in
            return true
        }

        repository.delete(by: "1")

        wait(for: [didDeleteExpectation], timeout: 0.5)

        let request: NSFetchRequest<CDFavoriteMeteor> = CDFavoriteMeteor.fetchRequest()
        let result = try! context.fetch(request)

        XCTAssert(result.isEmpty)
    }

    @discardableResult
    private func createTestMeteor(number: Int, context: NSManagedObjectContext) -> CDFavoriteMeteor {
        let meteor = CDFavoriteMeteor(context: context)
        meteor.id = "\(number)"
        meteor.lat = Double(number)
        meteor.lon = Double(number)
        meteor.mass = "\(number)"
        meteor.name = "\(number)"
        meteor.year = Date()

        return meteor
    }

}
