//
//  TestFavoriteRepository.swift
//  MeteorsTests
//
//  Created by Kristof Varga on 2021. 08. 02..
//

import Foundation
import XCTest
import Combine
@testable import Meteors

class TestFavoriteRepository: FavoriteRepository {

    // Input
    var meteor: Meteor?
    var meteors: [Meteor]?
    var error: Error?
    var expectation: XCTestExpectation?

    // Output
    var id: String?

    func fetchAll() -> AnyPublisher<[Meteor], Error> {
        if let error = error {
            return Fail(error: error)
                .eraseToAnyPublisher()
        } else if let meteors = meteors {
            return Just(meteors)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        } else {
            return Fail(error: TestError.noInputData)
                .eraseToAnyPublisher()
        }
    }

    func fetchById(id: String) -> AnyPublisher<Meteor?, Error> {
        self.id = id
        expectation?.fulfill()

        if let error = error {
            return Fail(error: error)
                .eraseToAnyPublisher()
        } else if let meteor = meteor {
            return Just(meteor)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        } else {
            return Fail(error: TestError.noInputData)
                .eraseToAnyPublisher()
        }
    }

    func add(meteor: Meteor) {
        self.meteor = meteor
        expectation?.fulfill()
    }

    func delete(by id: String) {
        self.meteor = nil
        expectation?.fulfill()
    }
}
