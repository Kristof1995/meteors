//
//  TestErr.swift
//  MeteorsTests
//
//  Created by Kristof Varga on 2021. 08. 02..
//

import Foundation

enum TestError: Error {
    case noInputData
    case testError
    case wrongType
}
