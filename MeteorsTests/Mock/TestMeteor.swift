//
//  TestMeteor.swift
//  MeteorsTests
//
//  Created by Kristof Varga on 2021. 08. 02..
//

import Foundation
@testable import Meteors

struct TestMeteor: Meteor {
    var meteorId: String
    var meteorName: String
    var mass: String?
    var lat: Double
    var lon: Double
    var year: Date?
}
