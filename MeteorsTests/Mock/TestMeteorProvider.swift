//
//  TestMeteorProvider.swift
//  MeteorsTests
//
//  Created by Kristof Varga on 2021. 08. 02..
//

import Foundation
import XCTest
import Combine
@testable import Meteors

class TestMeteorProvider: MeteorProvider {

    // Input
    var meteor: Meteor?
    var meteors: [Meteor]?
    var error: Error?

    var expectation: XCTestExpectation?

    // Output
    var orderQuery: OrderQuery?
    var id: String?
    var limit: Int?
    var offset: Int?

    func fetchMeteors(orderQuery: OrderQuery, limit: Int, offset: Int) -> AnyPublisher<[Meteor], Error> {
        self.orderQuery = orderQuery
        self.limit = limit
        self.offset = offset
        expectation?.fulfill()

        if let error = error {
            return Fail(error: error)
                .eraseToAnyPublisher()
        } else if let meteors = meteors {
            return Just(meteors)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        } else {
            return Fail(error: TestError.noInputData)
                .eraseToAnyPublisher()
        }
    }

    func fetchMeteor(byId id: String) -> AnyPublisher<Meteor, Error> {
        self.id = id
        expectation?.fulfill()

        if let error = error {
            return Fail(error: error)
                .eraseToAnyPublisher()
        } else if let meteor = meteor {
            return Just(meteor)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        } else {
            return Fail(error: TestError.noInputData)
                .eraseToAnyPublisher()
        }
    }
}
