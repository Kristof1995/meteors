//
//  TestNetworkConnector.swift
//  MeteorsTests
//
//  Created by Kristof Varga on 2021. 08. 02..
//

import Foundation
import Combine
import XCTest
@testable import Meteors

class TestNetworkConnector<Result: Codable>: NetworkConnector {
    // Input
    var resultArray: [Result]?
    var error: Error?
    var expectation: XCTestExpectation?

    // Output
    var request: URLRequest?

    func httpCall<T>(request: URLRequest) -> AnyPublisher<T, Error> where T : Decodable, T : Encodable {
        self.request = request
        expectation?.fulfill()

        if let error = error {
            return Fail(error: error).eraseToAnyPublisher()
        } else if let resultArray = resultArray {
            return Just(resultArray as! T)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        } else {
            return Fail(error: TestError.wrongType).eraseToAnyPublisher()
        }
    }
}
