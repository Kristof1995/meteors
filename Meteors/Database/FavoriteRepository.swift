//
//  FavoriteRepository.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 31..
//

import Foundation
import Combine

protocol FavoriteRepository {
    func fetchAll() -> AnyPublisher<[Meteor], Error>
    func fetchById(id: String) -> AnyPublisher<Meteor?, Error>
    func add(meteor: Meteor)
    func delete(by id: String)
}
