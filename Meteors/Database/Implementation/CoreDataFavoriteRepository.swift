//
//  CoreDataFavoriteRepository.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 31..
//

import Foundation
import Combine
import CoreData

class CoreDataFavoriteRepository: FavoriteRepository {

    private let persistentContainer: NSPersistentContainer

    init(persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
    }

    func fetchAll() -> AnyPublisher<[Meteor], Error> {
        let fetchRequest: NSFetchRequest<CDFavoriteMeteor> = CDFavoriteMeteor.fetchRequest()
        let viewContext = persistentContainer.viewContext

        return CoreDataPublisher(request: fetchRequest, context: viewContext)
            .flatMap( {
                Just($0)
                    .eraseToAnyPublisher()
            })
            .eraseToAnyPublisher()
    }

    func fetchById(id: String) -> AnyPublisher<Meteor?, Error> {
        let fetchRequest: NSFetchRequest<CDFavoriteMeteor> = CDFavoriteMeteor.fetchRequest()

        fetchRequest.predicate = NSPredicate(format: "id = %@", id)

        let viewContext = persistentContainer.viewContext

        return CoreDataPublisher(request: fetchRequest, context: viewContext)
            .flatMap({ meteors in
                Just(meteors.first)
            })
            .eraseToAnyPublisher()
    }

    func add(meteor: Meteor) {
        let context = persistentContainer.viewContext
        var entity: CDFavoriteMeteor!
        context.performAndWait {
            entity = meteor.createCoreDataEntity(with: context)

            do {
                try entity.managedObjectContext?.save()
            } catch {
                print("Unable to save. Error: \(error)")
            }
        }
    }

    func delete(by id: String) {
        let fetchRequest: NSFetchRequest<CDFavoriteMeteor> = CDFavoriteMeteor.fetchRequest()
        fetchRequest.predicate = .init(format: "id == %@", id)
        let context = persistentContainer.viewContext

        let result = try? context.fetch(fetchRequest)
        result?.forEach { context.delete($0 )}
        try? context.save()
    }
}

private extension Meteor {
    @discardableResult
    func createCoreDataEntity(with context: NSManagedObjectContext) -> CDFavoriteMeteor {
        let entity = CDFavoriteMeteor(context: context)
        entity.id = meteorId
        entity.name = meteorName
        entity.mass = mass
        entity.lon = lon
        entity.lat = lat
        entity.year = year
        return entity
    }
}
