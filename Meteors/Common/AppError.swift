//
//  AppError.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import Foundation

enum AppError: Error {
    case fileNotExists
    case unknown
    case notFound
    case nilFound
    case invalidQuery
}
