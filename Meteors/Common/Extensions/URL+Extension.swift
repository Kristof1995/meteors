//
//  URL+Extension.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import Foundation

extension URL {
    func addQueryParameter(key: String, value: String) -> URL {
        guard var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: false) else {
            return self
        }

        var queryItems = urlComponents.queryItems ?? [URLQueryItem]()

        queryItems.append(URLQueryItem(name: key, value: value))

        let encodedQuery = queryItems.map({ (queryItem) -> String in
            let key = queryItem.name
            let value = queryItem.value ?? ""
            let item = "\(key)=\(value.urlEncoded)"

            return item
        }).joined(separator: "&")

        urlComponents.percentEncodedQuery = encodedQuery

        return urlComponents.url!
    }
}
