//
//  String+Extension.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import Foundation

extension String {
    var urlEncoded: String {
        addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
}
