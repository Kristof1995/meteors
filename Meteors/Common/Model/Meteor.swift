//
//  Meteor.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 08. 02..
//

import Foundation

protocol Meteor {
    var meteorId: String { get }
    var meteorName: String { get }
    var mass: String? { get }
    var lat: Double { get }
    var lon: Double { get }
    var year: Date? { get }
}

extension NasaMeteor: Meteor {
    var meteorId: String {
        id
    }

    var meteorName: String {
        name
    }

    var lat: Double {
        guard let latStr = geolocation?.latitude, let lat = Double(latStr) else { return .zero }
        return lat
    }

    var lon: Double {
        guard let lonStr = geolocation?.longitude, let lon = Double(lonStr) else { return .zero }
        return lon
    }
}

extension CDFavoriteMeteor: Meteor {
    var meteorId: String {
        id ?? ""
    }

    var meteorName: String {
        name ?? ""
    }
}
