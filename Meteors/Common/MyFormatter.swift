//
//  MyFormatter.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 31..
//

import Foundation

class MyFormatter {
    static func formatMassInGramm(mass: String?) -> String {
        guard let mass = mass, let gramms = Double(mass) else { return "" }
        let massInGramms = Measurement(value: gramms, unit: UnitMass.grams)
        let formatter = MeasurementFormatter()
        formatter.unitOptions = .naturalScale
        formatter.unitStyle = .short
        return formatter.string(from: massInGramms)
    }

    static func formatDate(date: Date?) -> String {
        guard let date = date else { return "" }

        let formatter = DateFormatter()
        formatter.timeStyle = .none
        formatter.dateFormat = "YYYY"

        return formatter.string(from: date)
    }
}
