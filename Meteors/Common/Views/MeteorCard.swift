//
//  MeteorCard.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 31..
//

import SwiftUI

struct MeteorCard: View {
    var meteor: MeteorViewContent

    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 5) {
                Text(meteor.name)
                    .font(.title2)

                HStack(spacing: 10) {
                    Text(MyFormatter.formatMassInGramm(mass: meteor.mass))
                    HStack(spacing: 2) {
                        Image(systemName: "clock")
                        Text(MyFormatter.formatDate(date: meteor.year))
                    }
                }
                .font(.caption)
                .foregroundColor(.secondary)
            }

            Spacer()
        }
    }
}

struct MeteorCard_Previews: PreviewProvider {
    static var previews: some View {
        MeteorCard(meteor: .init(name: "Test meteor", id: "1", mass: "700", year: Date()))
            .previewLayout(.fixed(width: 350.0, height: 80.0))
    }
}
