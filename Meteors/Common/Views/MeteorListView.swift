//
//  MeteorListView.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 31..
//

import Foundation
import SwiftUI

struct MeteorViewContent: Identifiable, Equatable {
    let name: String
    let id: String
    let mass: String?
    let year: Date?
}

struct MeteorListView: View {

    var meteors: [MeteorViewContent]
    var isLoading: Bool
    var message: String
    var lastItemAppeared: (() -> Void)?

    var body: some View {
        ZStack {
            List {
                ForEach(meteors) { meteor in
                    NavigationLink(
                        destination: FeatureFactory.meteorDetail(meteorId: meteor.id),
                        label: {
                            MeteorCard(meteor: meteor)
                        })
                        .onAppear {
                            if meteors.last == meteor {
                                lastItemAppeared?()
                            }
                        }
                }
            }
            .listStyle(PlainListStyle())

            Text(message)

            if isLoading {
                ProgressView()
            }
        }
    }
}
