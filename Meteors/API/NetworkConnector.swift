//
//  NetworkConnector.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 08. 02..
//

import Foundation
import Combine

protocol NetworkConnector {
    func httpCall<T: Codable>(request: URLRequest) -> AnyPublisher<T, Error>
}
