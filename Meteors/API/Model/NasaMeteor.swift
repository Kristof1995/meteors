//
//  Meteor.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 29..
//

import Foundation

struct NasaMeteor: Codable, Identifiable {
    let name: String
    let id: String
    let nametype: String
    let recclass: String
    let mass: String?
    let fall: String
    let year: Date?
    let reclat: String?
    let reclong: String?
    let geolocation: GeoLocation?
}

struct GeoLocation: Codable {
    let latitude: String
    let longitude: String
}
