//
//  MeteorProvider.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 29..
//

import Foundation
import Combine

enum OrderQuery {
    case name(isAscending: Bool)
    case mass(isAscending: Bool)
    case location(location: (lat: Double, lon: Double), isAscending: Bool)
}

protocol MeteorProvider {
    func fetchMeteors(orderQuery: OrderQuery, limit: Int, offset: Int) -> AnyPublisher<[Meteor], Error>
    func fetchMeteor(byId id: String) -> AnyPublisher<Meteor, Error>
}
