//
//  NasaMeteorProvider.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 29..
//

import Foundation
import Combine

class NasaMeteorProvider {

    private let networkConnector: NetworkConnector
    private let baseUrl: URL

    init(baseUrl: URL, networkConnector: NetworkConnector) {
        self.baseUrl = baseUrl
        self.networkConnector = networkConnector
    }

    private func fetchNasaMeteors(orderQuery: OrderQuery, limit: Int, offset: Int) -> AnyPublisher<[NasaMeteor], Error> {
        var url = baseUrl
        url = url.addQueryParameter(key: "$where", value: "year > '1900-01-01T00:00:00.000'")
        url = url.addQueryParameter(key: "$order", value: orderQuery.queryString)
        url = url.addQueryParameter(key: "$limit", value: "\(limit)")
        url = url.addQueryParameter(key: "$offset", value: "\(offset)")

        let request = URLRequest(url: url)
        return networkConnector.httpCall(request: request)
            .eraseToAnyPublisher()
    }

    private func fetchNasaMeteor(byId id: String) -> AnyPublisher<NasaMeteor, Error> {
        var url = baseUrl
        url = url.addQueryParameter(key: "id", value: id)
        let request = URLRequest(url: url)
        return (networkConnector.httpCall(request: request) as AnyPublisher<[NasaMeteor], Error>)
            .tryMap({ meteors in
                guard let first = meteors.first else {
                    throw AppError.notFound
                }

                return first
            })
            .eraseToAnyPublisher()
    }
}

extension NasaMeteorProvider: MeteorProvider {
    func fetchMeteors(orderQuery: OrderQuery, limit: Int, offset: Int) -> AnyPublisher<[Meteor], Error> {
        fetchNasaMeteors(orderQuery: orderQuery, limit: limit, offset: offset)
            .map { $0 as [Meteor] }
            .eraseToAnyPublisher()
    }

    func fetchMeteor(byId id: String) -> AnyPublisher<Meteor, Error> {
        fetchNasaMeteor(byId: id)
            .map { $0 as Meteor }
            .eraseToAnyPublisher()
    }
}

extension OrderQuery {
    var queryString: String {
        switch self {
        case .name(isAscending: let isAscending):
            return "name " + (isAscending ? "ASC" : "DESC")
        case .mass(isAscending: let isAscending):
            return "mass " + (isAscending ? "ASC" : "DESC")
        case .location(location: let location, isAscending: let isAscending):
            return "DISTANCE_IN_METERS(geolocation, 'POINT(\(location.lon) \(location.lat))') " + (isAscending ? "ASC" : "DESC")
        }
    }
}
