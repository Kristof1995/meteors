//
//  NSURLSessionNetworkConnector.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 08. 02..
//

import Foundation
import Combine

class NSURLSessionNetworkConnector: NetworkConnector {
    func httpCall<T: Codable>(request: URLRequest) -> AnyPublisher<T, Error> {

        var request = request
        request.setValue(APIConstants.appToken, forHTTPHeaderField: "X-App-Token")
        request.setValue("application/json", forHTTPHeaderField: "Accept")

        print("Request sent: ", request)

        return URLSession.shared.dataTaskPublisher(for: request)
            .tryMap{ element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                    throw URLError(.badServerResponse)
                }
                return element.data
            }
            .decode(type: T.self, decoder: jsonDecoder())
            .eraseToAnyPublisher()
    }

    private func jsonDecoder() -> JSONDecoder {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }
}
