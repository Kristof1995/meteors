//
//  MeteorsApp.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 29..
//

import SwiftUI

@main
struct MeteorsApp: App {
    var body: some Scene {
        WindowGroup {
            #if TEST
            // This trick is necessary to calculate the real code coverage
            EmptyView()
            #else
            MainScreen()
            #endif
        }
    }
}
