//
//  FavoritesScreen.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import SwiftUI

struct FavoritesScreen: View {
    @StateObject var viewModel: FavoritesViewModel

    var body: some View {
        NavigationView {
            MeteorListView(meteors: viewModel.favorites.map({ $0.toViewContent() }), isLoading: .init(false), message: viewModel.message)
            .navigationTitle("Favorites")
        }
        .onAppear {
            viewModel.onAppear()
        }
    }
}

struct Favorite_Previews: PreviewProvider {
    static var previews: some View {
        FeatureFactory.favorites()
    }
}
