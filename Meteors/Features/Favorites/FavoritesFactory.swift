//
//  FavoritesFactory.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 31..
//

import Foundation
import CoreData

extension FeatureFactory {
    static func favorites() -> FavoritesScreen {
        let viewModel = FavoritesViewModel(favoriteRepository: CoreDataFavoriteRepository(persistentContainer: persistentContainer()))
        return .init(viewModel: viewModel)
    }

    static func persistentContainer() -> NSPersistentContainer {
        let container = NSPersistentContainer(name: "Favorites")
        container.loadPersistentStores { (description, error) in
            if let error = error {
                print("Error setting up Core Data \(error)")
            }
        }

        return container
    }
}
