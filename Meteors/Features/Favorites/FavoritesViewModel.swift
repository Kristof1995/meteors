//
//  FavoritesViewModel.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 31..
//

import Foundation
import Combine

class FavoritesViewModel: ObservableObject {
    @Published var favorites: [Meteor] = []
    @Published var message: String = ""

    private var firstAppear = true

    private var subscriptions: Set<AnyCancellable> = []

    private let favoriteRepository: FavoriteRepository

    init(favoriteRepository: FavoriteRepository) {
        self.favoriteRepository = favoriteRepository
    }

    func onAppear() {
        if firstAppear {
            firstAppear = false
            subscribe()
        }
    }

    private func subscribe() {

        subscriptions.removeAll()

        favoriteRepository.fetchAll()
            .replaceError(with: [])
            .sink(receiveCompletion: { completion in
                print("Favorites completion: ", completion)
            }, receiveValue: { [weak self] favorites in
                self?.favorites = favorites
            })
            .store(in: &subscriptions)

        $favorites
            .sink { [weak self] favorites in
                self?.message = favorites.isEmpty ? "You don't have any favorite meteor yet" : ""
            }
            .store(in: &subscriptions)
    }
}
