//
//  MainScreen.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import SwiftUI

struct MainScreen: View {
    var body: some View {
        TabView {
            FeatureFactory.meteorList()
                .tabItem { Label("Meteors", systemImage: "list.dash") }

            FeatureFactory.favorites()
                .tabItem { Label("Favorites", systemImage: "star") }
        }
    }
}

struct MainScreen_Previews: PreviewProvider {
    static var previews: some View {
        MainScreen()
    }
}
