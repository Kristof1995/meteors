//
//  MeteorListFactory.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import Foundation

extension FeatureFactory {
    static func meteorList() -> MeteorListScreen {
        let networkConnector = NSURLSessionNetworkConnector()
        let meteorProvider = NasaMeteorProvider(baseUrl: APIConstants.baseUrl, networkConnector: networkConnector)
        let viewModel = MeteorListViewModel(meteorProvider: meteorProvider)
        return .init(viewModel: viewModel)
    }
}
