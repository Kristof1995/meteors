//
//  MeteorListViewModel.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import Foundation
import Combine
import CoreLocation

enum Sorting {
    case name
    case mass
    case distance

    var text: String {
        switch self {
        case .name:
            return "Name"
        case .mass:
            return "Mass"
        case .distance:
            return "Distance"
        }
    }
}

class MeteorListViewModel: NSObject, ObservableObject {
    @Published var meteors: [Meteor] = []
    @Published var sorting: Sorting = .name
    @Published var isAscending: Bool = true
    @Published var isLoading = true
    @Published var message: String = ""

    @Published private var error: Error?

    private var refreshPublisher: CurrentValueSubject<Void, Never> = .init(())
    private var nextPagePublisher: PassthroughSubject<Void, Never> = .init()

    private  var page = 0
    private var pageLimit = 30

    private let meteorProvider: MeteorProvider

    private let locationManager = CLLocationManager()
    private var location: CLLocation?

    private var subscriptons: Set<AnyCancellable> = []

    init(meteorProvider: MeteorProvider) {
        self.meteorProvider = meteorProvider
    }

    func onAppear() {
        subscribe()
        requestLocationPresmission()
    }

    func refresh() {
        refreshPublisher.send()
    }

    func lastItemAppeared() {
        page += 1
        nextPagePublisher.send()
    }

    private func subscribe() {
        subscriptons.removeAll()
        subscribeToMeteors()
        subscribeToMessageEvents()
    }

    private func requestLocationPresmission() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }

    private func subscribeToMeteors() {
        Publishers.CombineLatest3($sorting, $isAscending, refreshPublisher)
            .handleEvents(receiveOutput: { [weak self] _, _, _ in
                self?.page = 0
                self?.meteors = []
            })
            .flatMap { [weak self] (sorting, isAscending, _) -> AnyPublisher<Result<[Meteor], Error>, Never> in
                guard let self = self else { return Just(.failure(AppError.nilFound)).eraseToAnyPublisher() }
                return self.fetch(sorting: sorting, isAscending: isAscending, page: self.page)
            }
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] result in
                self?.isLoading = false
                self?.handleResult(result: result)
            })
            .store(in: &subscriptons)


        nextPagePublisher
            .flatMap { [weak self] _ -> AnyPublisher<Result<[Meteor], Error>, Never> in
                guard let self = self else { return Just(.failure(AppError.nilFound)).eraseToAnyPublisher() }
                return self.fetch(sorting: self.sorting, isAscending: self.isAscending, page: self.page)
            }
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] result in
                self?.isLoading = false
                self?.handleResult(result: result)
            })
            .store(in: &subscriptons)
    }

    private func fetch(sorting: Sorting, isAscending: Bool, page: Int) -> AnyPublisher<Result<[Meteor], Error>, Never> {
        self.isLoading = true

        guard let orderQuery = self.convert(sorting: sorting, isAscending: isAscending, location: self.location) else {
            return Just(.failure(AppError.invalidQuery)).eraseToAnyPublisher()
        }

        let offset = page * self.pageLimit

        return self.meteorProvider.fetchMeteors(orderQuery: orderQuery, limit: self.pageLimit, offset: offset)
            .map { .success($0) }
            .catch { Just(.failure($0)) }
            .eraseToAnyPublisher()
    }

    private func handleResult(result: Result<[Meteor], Error>) {
        switch result {
        case .success(let meteors):
            self.addNewMeteors(newMeteors: meteors)
            self.error = nil
        case .failure(let error):
            self.meteors = []
            self.error = error
            print(error)
        }
    }

    private func subscribeToMessageEvents() {
        Publishers.CombineLatest3($meteors, $isLoading, $error)
            .sink { [weak self] meteors, isLoading, error in
                guard let self = self else { return }
                self.message = self.calculateMessage(meteors: meteors, error: error, isLoading: isLoading)
            }
            .store(in: &subscriptons)
    }

    private func addNewMeteors(newMeteors: [Meteor]) {
        newMeteors.forEach { meteor in
            if !self.meteors.contains(where: { $0.meteorId == meteor.meteorId }) {
                self.meteors.append(meteor)
            }
        }
    }

    private func calculateMessage(meteors: [Meteor], error: Error?, isLoading: Bool) -> String {
        if let error = error {
            return "Error occured: \(error.localizedDescription)"
        } else if meteors.isEmpty && !isLoading {
            return "No meteors found"
        } else {
            return ""
        }
    }

    private func convert(sorting: Sorting, isAscending: Bool, location: CLLocation?) -> OrderQuery? {
        switch sorting {
        case .name:
            return .name(isAscending: isAscending)
        case .mass:
            return .mass(isAscending: isAscending)
        case .distance:

            guard let lat = location?.coordinate.latitude, let lon = location?.coordinate.longitude else {
                return nil
            }

            return .location(location: (lat: lat, lon: lon), isAscending: isAscending)
        }
    }
}

extension MeteorListViewModel: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            manager.startUpdatingLocation()
        default:
            break
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        self.location = location
    }
}
