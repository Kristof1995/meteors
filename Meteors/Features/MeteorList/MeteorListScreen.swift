//
//  MeteorListScreen.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import SwiftUI

struct MeteorListScreen: View {

    @StateObject var viewModel: MeteorListViewModel

    var body: some View {
        NavigationView {
            VStack {
                SortingSelectorView(selected: $viewModel.sorting, isAscending: $viewModel.isAscending)
                    .padding()

                MeteorListView(meteors: viewModel.meteors.map { $0.toViewContent() }, isLoading: viewModel.isLoading, message: viewModel.message) {
                    viewModel.lastItemAppeared()
                }
            }
            .navigationTitle("Meteors")
            .toolbar {
                Button {
                    viewModel.refresh()
                } label: {
                    Image(systemName: "arrow.clockwise")
                }
            }
        }
        .onAppear {
            viewModel.onAppear()
        }
    }
}

struct SortingSelectorView: View {

    @Binding var selected: Sorting
    @Binding var isAscending: Bool

    var body: some View {
        HStack {
            Picker("", selection: $selected) {
                Text(Sorting.name.text).tag(Sorting.name)
                Text(Sorting.mass.text).tag(Sorting.mass)
                Text(Sorting.distance.text).tag(Sorting.distance)
            }.pickerStyle(SegmentedPickerStyle())

            Spacer()

            Button(action: {
                isAscending.toggle()
            }, label: {
                Image(systemName: isAscending ? "arrow.up.circle" : "arrow.down.circle")
            })
        }
    }
}

extension Meteor {
    func toViewContent() -> MeteorViewContent {
        .init(name: meteorName, id: meteorId, mass: mass, year: year)
    }
}

struct MeteorListScreen_Previews: PreviewProvider {
    static var previews: some View {
        FeatureFactory.meteorList()
    }
}
