//
//  MeteorDetailFactory.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import Foundation

extension FeatureFactory {
    static func meteorDetail(meteorId: String) -> MeteorDetailScreen {
        let networkConnector = NSURLSessionNetworkConnector()
        let meteorProvider = NasaMeteorProvider(baseUrl: APIConstants.baseUrl, networkConnector: networkConnector)
        let viewModel = MeteorDetailViewModel(meteorId: meteorId, meteorProvider: meteorProvider, favoriteRepository: CoreDataFavoriteRepository(persistentContainer: persistentContainer()))
        return .init(viewModel: viewModel)
    }
}
