//
//  MeteorDetailScreen.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import SwiftUI
import MapKit

struct MeteorDetailScreen: View {

    @StateObject var viewModel: MeteorDetailViewModel

    var body: some View {
        ZStack {
            if viewModel.message.isEmpty {
                Map(coordinateRegion: $viewModel.region, showsUserLocation: viewModel.showLocation, annotationItems: viewModel.pois) { item in
                    MapMarker(coordinate: item.coordinate)
                }
            } else {
                Text(viewModel.message)
            }

            if viewModel.isLoading {
                ProgressView()
            }
        }
        .navigationTitle(viewModel.meteor?.meteorName ?? "")
        .toolbar(content: {
            if viewModel.meteor != nil {
                Button {
                    viewModel.toggleFavorite()
                } label: {
                    Image(systemName: viewModel.isFavorite ? "heart.fill" : "heart")
                }
            }
        })
        .onAppear {
            viewModel.onAppear()
        }
    }
}

struct MeteorDetailScreen_Previews: PreviewProvider {
    static var previews: some View {
        FeatureFactory.meteorDetail(meteorId: "1")
    }
}
