//
//  MeteorDetailViewModel.swift
//  Meteors
//
//  Created by Kristof Varga on 2021. 07. 30..
//

import Foundation

import Combine
import MapKit

struct AnnotatedItem: Identifiable {
    let id = UUID()
    var name: String
    var coordinate: CLLocationCoordinate2D
}

class MeteorDetailViewModel: NSObject, ObservableObject {
    @Published var meteor: Meteor?
    @Published var region: MKCoordinateRegion = MKCoordinateRegion(center: .init(latitude: 0, longitude: 0), span: .init())
    @Published var pois: [AnnotatedItem] = []
    @Published var isLoading = true
    @Published var showLocation: Bool = false
    @Published var isFavorite = false
    @Published var message = ""

    private var firstAppear = true

    public let meteorId: String

    private let meteorProvider: MeteorProvider
    private let favoriteRepository: FavoriteRepository

    private var subscriptons: Set<AnyCancellable> = []
    private let locationManager = CLLocationManager()

    init(meteorId: String, meteorProvider: MeteorProvider, favoriteRepository: FavoriteRepository) {
        self.meteorId = meteorId
        self.meteorProvider = meteorProvider
        self.favoriteRepository = favoriteRepository
    }

    func onAppear() {
        if firstAppear {
            firstAppear = false

            subscriptons.removeAll()

            requestLocationPresmission()
            fetchMeteor()
            fetchFavorite()
        }
    }

    func toggleFavorite() {
        guard let meteor = meteor else { return }
        self.setFavorite(meteor: meteor, to: !isFavorite)
    }

    private func requestLocationPresmission() {
        locationManager.delegate = self
        update(with: locationManager.authorizationStatus)
        locationManager.requestWhenInUseAuthorization()
    }

    private func fetchMeteor() {
        if meteor != nil {
            return
        }

        isLoading = true

        meteorProvider.fetchMeteor(byId: meteorId)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                self?.isLoading = false
                switch completion {
                case .finished:
                    print("fetch finished")
                case .failure(let error):
                    print("fetch finished with error: ", error)
                    self?.message = "Error happened: \(error)"
                }
            } receiveValue: { [weak self] meteor in
                self?.update(with: meteor)
            }
            .store(in: &subscriptons)
    }

    private func fetchFavorite() {
        favoriteRepository.fetchById(id: meteorId)
            .replaceError(with: nil)
            .map { $0 == nil ? false : true }
            .sink(receiveCompletion: { completion in
                print(completion)
            }, receiveValue: { [weak self] isFavorite in
                self?.isFavorite = isFavorite
            })
            .store(in: &subscriptons)
    }

    private func update(with meteor: Meteor) {

        self.message = ""
        self.meteor = meteor
        let lat = meteor.lat
        let lon = meteor.lon

        region = .init(center: .init(latitude: lat, longitude: lon), latitudinalMeters: 1000000, longitudinalMeters: 1000000)
        pois = [ .init(name: meteor.meteorName, coordinate: .init(latitude: lat, longitude: lon))]
    }

    private func update(with authorizationStatus: CLAuthorizationStatus) {
        switch authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            showLocation = true
        default:
            showLocation = false
        }
    }

    private func setFavorite(meteor: Meteor, to isFavorite: Bool) {
        if isFavorite {
            favoriteRepository.add(meteor: meteor)
        } else {
            favoriteRepository.delete(by: meteorId)
        }
    }
}

extension MeteorDetailViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        update(with: status)
    }
}
